module gitlab.com/jackatbancast/manifesto

go 1.16

require (
	github.com/go-git/go-git/v5 v5.2.0
	github.com/google/go-jsonnet v0.17.0
	github.com/hashicorp/go-multierror v1.0.0
	github.com/manifoldco/promptui v0.8.0 // indirect
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/objx v0.3.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/api v0.21.0
	k8s.io/apimachinery v0.21.0
	k8s.io/client-go v0.21.0
)
