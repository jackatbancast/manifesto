local template = import 'core/templates.libsonnet';
local source = template.sourceType;
local parameter = template.parameterType;

template.new('service') +
template.mixin.metadata.withMaintainer('test@example.com') +
template.mixin.metadata.withParametersMixin([
  parameter.string.new('description') +
  parameter.withDefault('This service is going be a very disruptive service'),
  parameter.group.new('environments', [
    parameter.string.new('name'),
    parameter.string.new('cluster'),
    parameter.string.new('namespace'),
    parameter.string.new('release'),
  ]),
  parameter.string.new('image') +
  parameter.string.withDefault('nginx:alpine'),
]) +
template.mixin.spec.withSourcesMixin([
  source.goTemplate.new('README.md', 'sources/README.md.tmpl'),
  source.goTemplate.new('metadata.jsonnet', 'sources/metadata.jsonnet.tmpl'),
  source.directory.new('app'),
  source.goTemplate.new('app/app.libsonnet', 'sources/app.libsonnet.tmpl'),
  source.directory.new('instances'),
] + std.flattenArrays([
  [
    source.directory.new('instances/%(cluster)s' % environment),
    source.directory.new('instances/%(cluster)s/%(namespace)s' % environment),
    source.goTemplate.new(
      'instances/%(cluster)s/%(namespace)s/%(release)s.jsonnet' % environment,
      'sources/instance.jsonnet.tmpl',
    ),
  ]
  for environment in template.getParameter('environments')
]))
