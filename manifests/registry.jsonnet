local registry = import 'core/registry.libsonnet';

registry {
  local cluster = $.cluster,

  teams: [],

  services: (import './services/services.jsonnet'),

  clusters: [
    cluster.new(
      'k3s',  // Name
      'https://127.0.0.1:6443',  // Endpoint
      '',  // Cert
      '',  // Key
      '',  // CA Cert
    ),
  ],
}
