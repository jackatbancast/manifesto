{
  cluster+:: $.base {
    apiVersion: 'manifesto.k8s.jackstephenson.dev/v1alpha1',
    kind: 'Cluster',
    metadata+: {
      name: error 'cluster.name required',
      description: null,
      cert: error 'required',
      key: error 'required',
      ca: error 'required',
    },
    spec+: {
      endpoint: error 'cluster.endpoint required',
    },
    rbac+: $.rbac,

    new(name, endpoint, cert, key, ca)::
      self +
      self.mixin.metadata.withName(name) +
      self.mixin.spec.withEndpoint(endpoint) +
      self.mixin.metadata.withCert(cert) +
      self.mixin.metadata.withKey(key) +
      self.mixin.metadata.withCA(ca),

    mixin+:: {
      metadata+:: {
        local __metadataMixin(m) = { metadata+: m },
        withName(name):: __metadataMixin({ name: name }),
        withDescription(description):: __metadataMixin({ description: description }),
        withCert(cert):: __metadataMixin({ cert: cert }),
        withKey(key):: __metadataMixin({ key: key }),
        withCA(ca):: __metadataMixin({ ca: ca }),
      },
      spec+:: {
        local __specMixin(s) = { spec+: s },
        withEndpoint(endpoint):: __specMixin({ endpoint: endpoint }),
      },
    },
  },
}
