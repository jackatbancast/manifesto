{
  team+:: $.base {
    apiVersion: 'team.k8s.jackstephenson.dev/v1alpha1',
    kind: 'Team',
    metadata+: {
      name: error 'required',
      provider: null,
    },
    spec+: {
      group_id: null,
    },

    new(name, groupID=null)::
      self +
      self.mixin.metadata.withName(name) +
      self.mixin.spec.withGroupID(groupID),

    mixin+:: {
      metadata+:: {
        local __metadataMixin(m) = { metadata+: m },
        withName(name):: __metadataMixin({ name: name }),
        withProvider(provider):: __metadataMixin({ provider: provider }),
      },
      spec+:: {
        local __specMixin(s) = { spec+: s },
        withGroupID(groupID):: __specMixin({ group_id: groupID }),
      },
    },
  },

}
