{
  rbac+:: {
    local __rbacMixin(r) = { rbac+: r },
    local this = self,
    spec+: {
      admins+: [],
      operators+: [],
      viewers+: [],
    },

    transitive+: {
      spec+: {
        admins: std.set(this.spec.admins),
        operators: std.set(this.spec.operators + self.admins),
        viewers: std.set(this.spec.viewers + self.operators),
      },
    },

    mixin+:: {
      spec+:: {
        local __specMixin(s) = __rbacMixin({ spec+: s }),
        withAdminsMixin(admins):: __specMixin({ admins+: admins }),
        withOperatorsMixin(operators):: __specMixin({ operators+: operators }),
        withViewersMixin(viewers):: __specMixin({ viewers+: viewers }),
      },
    },
  },
}
