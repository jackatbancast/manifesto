{
  service+:: $.base {
    apiVersion: 'manifesto.k8s.jackstephenson.dev/v1alpha1',
    kind: 'Service',
    metadata: {
      name: error 'service.name required',
      description: null,
    },
    spec+: {
      environments: [],
    },
    rbac+: $.rbac,

    new(name)::
      self +
      self.mixin.metadata.withName(name),

    mixin+:: {
      metadata+:: {
        local __metadataMixin(m) = { metadata+: m },
        withName(name):: __metadataMixin({ name: name }),
        withDescription(description):: __metadataMixin({ description: description }),
        withTeam(team):: __metadataMixin({ team: team }),
      },
      spec+:: {
        local __specMixin(s) = { spec+: s },
        withEnvironments(environments):: __specMixin({ environments: environments }),
      },
    },

    environment+:: $.base {
      apiVersion: 'manifesto.k8s.jackstephenson.dev/v1alpha1',
      kind: 'Environment',
      metadata: {
        name: error 'environment.name required',
      },
      spec+: {
        targets: [],
        autoSync: false,
        autoPrune: false,
      },
      rbac+: $.rbac,

      new(name)::
        self +
        self.mixin.metadata.withName(name),

      mixin+:: {
        metadata+:: {
          local __metadataMixin(m) = { metadata+: m },
          withName(name):: __metadataMixin({ name: name }),
        },
        spec+:: {
          local __specMixin(s) = { spec+: s },
          withTargets(targets):: __specMixin({ targets: targets }),
          withAutoSync(autoSync):: __specMixin({ autoSync: autoSync }),
          withAutoPrune(autoPrune):: __specMixin({ autoPrune: autoPrune }),
        },
      },

      target+:: $.base {
        local target = self,

        apiVersion: 'manifesto.k8s.jackstephenson.dev/v1alpha1',
        kind: 'Target',
        metadata+: {
          type: error 'required',
          context: error 'required',
          name: error 'required',
        },
        spec+: {},
        rbac+: $.rbac,

        new(type, context, name)::
          self +
          self.mixin.metadata.withType(type) +
          self.mixin.metadata.withContext(context) +
          self.mixin.metadata.withName(name),

        mixin+:: {
          metadata+:: {
            local __metadataMixin(m) = { metadata+: m },
            withName(name):: __metadataMixin({ name: name }),
            withType(type):: __metadataMixin({ type: type }),
            withContext(context):: __metadataMixin({ context: context }),
          },
        },

        argocd+:: target {
          new(context, namespace, name)::
	    self +
            super.new('ArgoCD', context, name) +
            self.mixin.spec.withNamespace(namespace),

          spec+: {
            namespace: error 'required',
          },

          mixin+:: {
            spec+:: {
              local __specMixin(s) = { spec+: s },
              withNamespace(namespace):: __specMixin({ namespace: namespace}),
            },
          },
        },
      },
    },
  },
}
