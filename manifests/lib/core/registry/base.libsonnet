{
  enumerate+:: {
    services+:: $.services,
    environments+:: std.flattenArrays([[service { environment: environment, rbac+: environment.rbac } for environment in service.spec.environments] for service in $.enumerate.services]),
    targets+:: std.flattenArrays([[service { target: target, rbac+: target.rbac } for target in service.environment.spec.targets] for service in $.enumerate.environments]),
  },

  find+:: {
    local finder = function(array, name) std.filter(function(o) o.metadata.name == name, array)[0],

    cluster(name):: finder($.clusters, name),
    team(name):: finder($.teams, name),
  },

  base+:: {
    map(defaults, func):: func(defaults),
    flatten(arrays):: std.flattenArrays(arrays),
  },
}
