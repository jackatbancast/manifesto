{
  new(name):: self + self.mixin.metadata.withName(name),

  metadata+: {
    name: error 'please name your template',
    maintainer: null,
    parameters: [],
  },

  spec+: {
    sources: [],
  },

  mixin+:: {
    metadata+: {
      local __metadataMixin(m) = { metadata+: m },
      withName(name):: __metadataMixin({ name: name }),
      withMaintainer(maintainer):: __metadataMixin({ maintainer: maintainer }),
      withParameters(parameters):: __metadataMixin({ parameters: parameters }),
      withParametersMixin(parameters):: __metadataMixin({ parameters+: parameters }),
    },
    spec+: {
      local __specMixin(s) = { spec+: s },
      withSources(sources):: __specMixin({ sources: sources }),
      withSourcesMixin(sources):: __specMixin({ sources+: sources }),
    },
  },

  getParameter(name):: (
    // if std.extVar('RENDER_SOURCES')
    // then
      std.extVar('Param::%s' % name)
  ),

  parameterType+:: {
    name: error 'must have a parameter name',
    type: error 'must have a parameter type',
    default: null,
    required: null,
    children: null,

    new(type):: self + self.withType(type) {
      new(name, required=false)::
        self +
        self.withName(name) +
        self.withRequired(required),
    },

    withName(name):: { name: name },
    withType(type):: { type: type },
    withDefault(default):: { default: default },
    withRequired(required):: { required: required },
    withChildren(children):: { children: children },
    withChildrenMixin(children):: { children+: children },

    string:: self.new('String'),
    integer:: self.new('Integer'),
    float:: self.new('Float'),
    boolean:: self.new('Boolean'),

    group:: self.new('Group') {
      new(name, children)::
        super.new(name, required=false) +
        self.withChildren(children),
    },
  },

  sourceType+:: {
    metadata+: {
      type: error 'plase specify a type name',
      target: error 'must have a target file',
      conditions: [],
      depedencies: [],
    },
    spec+: {},

    new(type):: self + self.mixin.metadata.withType(type),

    mixin+:: {
      metadata+: {
        local __metadataMixin(m) = { metadata+: m },
        withType(type):: __metadataMixin({ type: type }),
        withTarget(target):: __metadataMixin({ target: target }),
        withConditions(conditions):: __metadataMixin({ conditions: conditions }),
        withConditionsMixin(conditions):: __metadataMixin({ conditions+: conditions }),
        withDependecies(dependencies):: __metadataMixin({ dependencies: dependencies }),
        withDependeciesMixin(dependencies):: __metadataMixin({ dependencies+: dependencies }),
      },
      spec+: {
        local __specMixin(s) = { spec+: s },
      },
    },

    directory+:: self.new('Directory') {
      new(target)::
        self +
        self.mixin.metadata.withTarget(target),
    },
    file+:: self.new('File') {
      new(target, source)::
        self +
        self.mixin.metadata.withTarget(target) +
        self.mixin.spec.withSource(source),

      spec+: {
        source: error 'must source a file',
      },

      mixin+:: {
        spec+: {
          local __specMixin(s) = { spec+: s },
          withSource(source):: __specMixin({ source: source }),
        },
      },
    },
    goTemplate+:: self.new('GoTemplate') {
      new(target, source)::
        self +
        self.mixin.metadata.withTarget(target) +
        self.mixin.spec.withSource(source),

      spec+: {
        source: error 'must source a file',
      },

      mixin+:: {
        spec+: {
          local __specMixin(s) = { spec+: s },
          withSource(source):: __specMixin({ source: source }),
        },
      },
    },
    jsonnetTemplate+:: self.new('JsonnetTemplate') {
      new(target, source)::
        self +
        self.mixin.metadata.withTarget(target) +
        self.mixin.spec.withSource(source),

      spec+: {
        source: error 'must source a file',
      },

      mixin+:: {
        spec+: {
          local __specMixin(s) = { spec+: s },
          withSource(source):: __specMixin({ source: source }),
        },
      },
    },
  },
}
