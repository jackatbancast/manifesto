(import './registry/base.libsonnet') +
(import './registry/rbac.libsonnet') +
(import './registry/team.libsonnet') +
(import './registry/cluster.libsonnet') +
(import './registry/service.libsonnet')
