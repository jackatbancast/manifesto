local app = import '../../../app/app.libsonnet';

app {
  _config+:: {
    labels+: {
      extra_label: 'some-extra-label',
    },
  },
}
