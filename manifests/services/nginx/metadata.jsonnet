local registry = import 'core/registry.libsonnet';
local service = registry.service;
local environment = service.environment;
local target = environment.target;

service.new('nginx') +
service.mixin.spec.withEnvironments([
  environment.new('production') +
  environment.mixin.spec.withTargets([
    target.argocd.new('k3s', 'example', 'default'),
  ]),
])
