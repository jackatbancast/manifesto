local core = import 'core/workload.libsonnet';

core {
  local cfg = $._config,
  _config+:: {
    service: std.extVar('service'),
    environment: std.extVar('environment'),
    namespace: std.extVar('namespace'),
    release: std.extVar('release'),

    tag: 'latest',

    labels: {
      service: cfg.service,
      environment: cfg.environment,
      namespace: cfg.namespace,
      release: cfg.release,
    },
  },

  local deployment = $.apps.v1.deployment,
  local container = $.core.v1.container,

  resources+: {
    server:
      deployment.new(
        'example-deployment',
        containers=[
          container.new('nginx', image='nginx:%s' % cfg.tag),
        ],
      ) +
      deployment.mixin.metadata.withNamespace(cfg.namespace) +
      deployment.mixin.metadata.withLabelsMixin(cfg.labels),
  },
}
