{
  mount_points: {
    '/': {
      is_string: true,
      snippet: '"Hello world %s" % (1+1)',
    },
    '/metrics': {
      is_string: true,
      filename: 'metrics.jsonnet',
    },
    '/playground': {
      filename: 'playground.jsonnet',
    },
    '/gol': {
      filename: 'gol.jsonnet',
      is_string: true,
    },
    '/gol-init': {
      filename: 'golInit.jsonnet',
      is_string: true,
    }
  },
  address: ':8080',
}
