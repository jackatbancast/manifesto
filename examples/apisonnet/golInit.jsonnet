local boardName = 'board|%s' % std.extVar('header|Board-Name');
local currentBoard = std.native('memoryGet')(boardName);

local displayBoard(obj) = (
  '%s\n%s' % [
    obj.saved,
    std.join('\n', [
      std.foldl(
        function(i, a) (
          i + (
            if a == 1
            then 'O'
            else ' '
          )
        ),
        row,
        '',
      )
      for row in obj.board
    ]),
  ]
);

local parseBoard(board) = [
  [
    (
      if col != '0'
      then 1
      else 0
    )
    for col in std.stringChars(row)
  ]
  for row in std.split(board, '|')
];

if currentBoard == null
then (
  local board = parseBoard(std.extVar('request|body'));
  displayBoard({
    board: board,
    saved: std.native('memoryPut')(boardName, board),
  })
)
else 'board %s already exists' % std.extVar('header|Board-Name')
