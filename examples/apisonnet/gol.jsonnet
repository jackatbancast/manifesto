local displayBoard(obj) = (
  '%s\n%s' % [
    obj.saved,
    std.join('\n', [
      std.foldl(
        function(i, a) (
          i + (
            if a == 1
            then 'O'
            else ' '
          )
        ),
        row,
        '',
      )
      for row in obj.board
    ]),
  ]
);

displayBoard({
  local boardName = 'board|%s' % std.extVar('header|Board-Name'),
  local initialBoardState = (
    local board = std.native('memoryGet')(boardName);
    local initialBoard = (
      if board != null
      then board
      else [
        [
          // 00000
          // 00000
          // 01110
          // 00000
          // 00000
          //
          // 00000
          // 00100
          // 00100
          // 00100
          // 00000
          (
            if x < 4 && x > 0 && y == 2
            then 1
            else 0
          )
          for x in std.range(0, 4)
        ]
        for y in std.range(0, 4)
      ]
    );

    initialBoard
  ),
  local size = {
    y: std.length(initialBoardState),
    x: std.length(initialBoardState[0]),
  },
  local newState(x, y) = (
    local alive = initialBoardState[y][x];
    local countSurrounding = std.foldl(
      function(i, a) i + a,
      std.flattenArrays(
        [
          [
            (
              if (
                (ix != 0) || (iy != 0)
              ) && (
                ((x + ix) > -1) && ((y + iy) > -1)
              ) && (
                ((x + ix) < size.x) && ((y + iy) < size.y)
              )
              then initialBoardState[y + iy][x + ix]
              // then '%s:%s|' % [x + ix, y + iy]
              else 0
            )
            for ix in std.range(-1, 1)

          ]
          for iy in std.range(-1, 1)
        ]
      ),
      0,
      // '',
    );

    // countSurrounding
    // * Any live cell with two or three live neighbours survives.
    // * Any dead cell with three live neighbours becomes a live cell.
    // * All other live cells die in the next generation. Similarly, all other dead cells stay dead.
    (
      if (alive == 1 && std.member(std.range(2, 3), countSurrounding)) || (alive == 0 && countSurrounding >= 3)
      then 1
      else 0
    )
  ),
  local newBoardState = [
    [
      newState(x, y)
      for x in std.range(0, size.x - 1)
    ]
    for y in std.range(0, size.y - 1)
  ],
  board: newBoardState,
  saved: std.native('memoryPut')(boardName, newBoardState),
})
