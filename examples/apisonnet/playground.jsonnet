{
  local header = std.extVar('header|Name'),
  // hello: 'twitch community %s' % header,
  // google: std.native('httpGet')(std.extVar('header|Get-This')),
  
  put_response: std.native('memoryPut')('hello', {val: $.val.val + 1}),
  val: (
    local val = std.native('memoryGet')('hello');
    if val != null
    then val
    else {val: 0}
  ),
}
