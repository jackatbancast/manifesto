local counter(name, values) = '%(name)s{%(values)s} 1' % {
  name: name,
  values: std.join(',', ['%s="%s"' % [key, values[key]] for key in std.objectFields(values)]),
};

// manifesto_service{service="prometheus", envrionment="doke", cluster="doke"} 1
std.join('\n', std.flattenArrays([
  [
    counter('manifesto_service', service)
    for service in [
      { service: 'abc' },
      { service: '123' },
    ]
  ],
]))
