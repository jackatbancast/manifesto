package registry

import "github.com/stretchr/objx"

type Registry struct {
	Services []Service `json:"services"`
}

func (registry *Registry) Find(service string) *Service {
	for _, svc := range registry.Services {
		if svc.Meta.Name == service {
			return &svc
		}
	}

	return nil
}

type Service struct {
	Meta ServiceMeta `json:"metadata"`
	Spec ServiceSpec `json:"spec"`
}

func (service *Service) Find(environment string) *Environment {
	for _, env := range service.Spec.Environments {
		if env.Meta.Name == environment {
			return &env
		}
	}

	return nil
}

type ServiceMeta struct {
	Name string `json:"name"`
}

type ServiceSpec struct {
	Environments []Environment `json:"environments"`
}

type Environment struct {
	Meta EnvironmentMeta `json:"metadata"`
	Spec EnvironmentSpec `json:"spec"`
}

func (environment *Environment) Find(targetType, target string) *Target {
	for _, tgt := range environment.Spec.Targets {
		if (targetType == "" || tgt.Meta.Type == targetType) && tgt.Meta.Name == target {
			return &tgt
		}
	}

	return nil
}

type EnvironmentMeta struct {
	Name string `json:"name"`
}

type EnvironmentSpec struct {
	Targets []Target `json:"targets"`
}

type Target struct {
	Meta TargetMeta `json:"metadata"`
	// The spec of targets is potentially in flux, handle this by
	// taking in a map
	Spec objx.Map `json:"spec,omitempty"`
}

type TargetMeta struct {
	Type    string `json:"type"`
	Context string `json:"context"`
	Name    string `json:"name"`
}
