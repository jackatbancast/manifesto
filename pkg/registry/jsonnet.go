package registry

import (
	"context"
	"encoding/json"

	"gitlab.com/jackatbancast/manifesto/pkg/jsonnetenv"
)

func GetRegistry(ctx context.Context, jpaths []string, registryPath string) (*Registry, error) {
	vm, err := jsonnetenv.NewVMWithOptions(ctx, ".", &jsonnetenv.VMOptions{
		JPaths:   jpaths,
		Snapshot: true,
	})
	if err != nil {
		return nil, err
	}
	return GetRegistryVM(ctx, vm, registryPath)
}

func GetRegistryVM(ctx context.Context, vm *jsonnetenv.VM, registryPath string) (*Registry, error) {
	jsonOutput, err := vm.EvaluateFile(ctx, registryPath)
	if err != nil {
		return nil, err
	}

	// Deserialise registry into type
	var reg Registry
	err = json.Unmarshal([]byte(jsonOutput), &reg)
	if err != nil {
		return nil, err
	}

	return &reg, nil
}
