package jsonnetenv

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func WithHTTPGet() VMOption {
	return WithNativeFunction("httpGet", []string{"address"}, func(params []interface{}) (interface{}, error) {
		address, ok := params[0].(string)
		if !ok {
			return nil, fmt.Errorf("failed to parse address")
		}

		resp, err := http.Get(address)
		if err != nil {
			return map[string]interface{}{
				"error": err.Error(),
			}, nil
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		return map[string]interface{}{
			"body": string(body),
			"code": fmt.Sprintf("%d", resp.StatusCode),
		}, nil
	})
}
