package jsonnetenv

import (
	"context"
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	jsonnet "github.com/google/go-jsonnet"
	"github.com/google/go-jsonnet/ast"
	"gitlab.com/jackatbancast/manifesto/pkg/kubernetes"
	manifestsLib "gitlab.com/jackatbancast/manifesto/pkg/kubernetes/manifests"
	"gitlab.com/jackatbancast/manifesto/pkg/secrets"
)

func WithNativeFunction(name string, params []string, function func([]interface{}) (interface{}, error)) VMOption {
	return func(_ context.Context, vm *VM) error {
		identifiers := make([]ast.Identifier, 0, len(params))
		for _, param := range params {
			identifiers = append(identifiers, ast.Identifier(param))
		}

		vm.VM.NativeFunction(&jsonnet.NativeFunction{
			Func:   function,
			Params: identifiers,
			Name:   name,
		})

		return nil
	}
}

func WithExtractManifestsNativeFunction() VMOption {
	return WithNativeFunction("extractManifests", []string{"m"}, func(m []interface{}) (interface{}, error) {
		manifests := manifestsLib.ExtractManifestObjects(m)

		ms := make([]interface{}, 0, len(manifests))
		for _, doc := range manifests {
			ms = append(ms, doc)
		}
		return ms, nil
	})
}

func WithGitRevisionNativeFunction() VMOption {
	return WithNativeFunction("gitRevision", []string{"rev"}, func(params []interface{}) (interface{}, error) {
		revString, ok := params[0].(string)
		if !ok {
			return nil, fmt.Errorf("rev is not a string")
		}

		cwd, err := os.Getwd()
		if err != nil {
			return nil, err
		}

		repo, err := git.PlainOpenWithOptions(cwd, &git.PlainOpenOptions{DetectDotGit: true})
		if err != nil {
			return nil, err
		}

		ref, err := repo.ResolveRevision(plumbing.Revision(revString))
		if err != nil {
			return nil, err
		}

		return ref.String(), nil
	})
}

func WithReadSecretNativeFunction(relativePath string, isSnapshot bool) VMOption {
	return WithNativeFunction("readSecret", []string{"filepath"}, func(params []interface{}) (interface{}, error) {
		return ReadSecret(
			context.Background(),
			os.Getenv("KUBERNETES_CONTEXT"),
			secrets.DefaultKubernetesSecretName,
			filepath.Join(relativePath, params[0].(string)),
			isSnapshot,
		)
	})
}

func WithReadSecretContextNativeFunction(relativePath string, isSnapshot bool) VMOption {
	return WithNativeFunction("readSecretContext", []string{"context", "secret", "filepath"}, func(params []interface{}) (interface{}, error) {
		return ReadSecret(
			context.Background(),
			params[0].(string),
			params[1].(string),
			filepath.Join(relativePath, params[2].(string)),
			isSnapshot,
		)
	})
}

func ReadSecret(ctx context.Context, kubeContext string, kubeSecret string, filePath string, isSnapshot bool) (interface{}, error) {
	if isSnapshot {
		bytes, err := os.ReadFile(filePath)
		if err != nil {
			return nil, fmt.Errorf("failed to read file: %w", err)
		}
		shasum := sha512.Sum512_256(bytes)
		return base64.RawStdEncoding.EncodeToString(shasum[:]), nil
	}

	client, err := kubernetes.GetKubeClient(kubeContext)
	if err != nil {
		return nil, err
	}

	output, err := secrets.DecryptSecretFile(ctx, client, kubeSecret, filePath)
	if err != nil {
		return nil, err
	}

	return output, nil
}
