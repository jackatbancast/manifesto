package jsonnetenv

import (
	"context"
	"path/filepath"

	jsonnet "github.com/google/go-jsonnet"
)

type VMOption func(context.Context, *VM) error

func WithFileImporter(basepath string, jpaths []string) VMOption {
	basepath, err := filepath.Abs(basepath)
	basedir := filepath.Dir(basepath)
	if err != nil {
		return nil
	}

	return withJPaths(append([]string{basedir}, jpaths...))
}

func withJPaths(jpaths []string) VMOption {
	return func(_ context.Context, vm *VM) error {
		vm.VM.Importer(&jsonnet.FileImporter{
			JPaths: jpaths,
		})
		return nil
	}
}

func WithStringOutput(output bool) VMOption {
	return func(_ context.Context, vm *VM) error {
		vm.VM.StringOutput = output
		return nil
	}
}

type Var struct {
	Key   string
	Value string
}

func WithExtVars(vars []Var) VMOption {
	return func(_ context.Context, vm *VM) error {
		for _, extVar := range vars {
			vm.VM.ExtVar(extVar.Key, extVar.Value)
		}

		return nil
	}
}

func WithExtCodes(codes []Var) VMOption {
	return func(_ context.Context, vm *VM) error {
		for _, extVar := range codes {
			vm.VM.ExtCode(extVar.Key, extVar.Value)
		}

		return nil
	}
}

func WithTLAVars(vars []Var) VMOption {
	return func(c_ context.Context, vm *VM) error {
		for _, tla := range vars {
			vm.VM.TLAVar(tla.Key, tla.Value)
		}

		return nil
	}
}
