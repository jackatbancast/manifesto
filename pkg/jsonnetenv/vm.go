package jsonnetenv

import (
	"context"
	"io/ioutil"

	jsonnet "github.com/google/go-jsonnet"
)

func NewVM(ctx context.Context, relativePath string, isSnapshot bool, extVars []Var, tlaVars []Var) *VM {
	vm := &VM{jsonnet.MakeVM()}
	vm.Apply(
		ctx,
		WithExtractManifestsNativeFunction(),
		WithGitRevisionNativeFunction(),
		WithReadSecretNativeFunction(relativePath, isSnapshot),
		WithReadSecretContextNativeFunction(relativePath, isSnapshot),
		WithExtVars(extVars),
		WithTLAVars(tlaVars),
	)
	return vm
}

// NewVMWithOptions creates a new
func NewVMWithOptions(ctx context.Context, relativePath string, options *VMOptions) (*VM, error) {
	vm := &VM{jsonnet.MakeVM()}
	err := vm.Apply(
		ctx,
		WithExtractManifestsNativeFunction(),
		WithGitRevisionNativeFunction(),
		WithReadSecretNativeFunction(relativePath, options.Snapshot),
		WithReadSecretContextNativeFunction(relativePath, options.Snapshot),
		WithFileImporter(relativePath, options.JPaths),
		WithExtVars(options.ExtVars),
		WithTLAVars(options.TLAVars),
	)

	return vm, err
}

type VM struct {
	*jsonnet.VM
}

type VMOptions struct {
	JPaths   []string
	Snapshot bool
	ExtVars  []Var
	TLAVars  []Var
}

func (vm *VM) EvaluateFile(ctx context.Context, path string) (string, error) {
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return vm.EvaluateSnippet(ctx, path, string(contents))
}

func (vm *VM) EvaluateSnippet(_ context.Context, path string, snippet string) (string, error) {
	output, err := vm.VM.EvaluateSnippet(path, snippet)
	if err != nil {
		return output, err
	}

	return output, nil
}

func (vm *VM) Apply(ctx context.Context, opts ...VMOption) error {
	for _, opt := range opts {
		err := opt(ctx, vm)
		if err != nil {
			return err
		}
	}

	return nil
}
