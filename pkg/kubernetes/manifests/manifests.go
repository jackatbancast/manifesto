package manifests

import (
	"bytes"
	"encoding/json"

	"github.com/hashicorp/go-multierror"
	"gopkg.in/yaml.v2"
)

func ExtractManifestObjects(obj interface{}) []interface{} {
	manifests := []interface{}{}

	if objList, ok := obj.([]interface{}); ok {
		for _, o := range objList {
			manifests = append(manifests, ExtractManifestObjects(o)...)
		}
	}

	if objMap, ok := obj.(map[string]interface{}); ok {
		_, hasKind := objMap["kind"]
		_, hasApiVersion := objMap["apiVersion"]
		if hasKind && hasApiVersion {
			return []interface{}{objMap}
		}

		for _, val := range objMap {
			manifests = append(manifests, ExtractManifestObjects(val)...)
		}
	}

	return manifests

}

func ExtractJSONManifests(j string) ([]interface{}, error) {
	var jo interface{}

	err := json.Unmarshal([]byte(j), &jo)
	if err != nil {
		return nil, err
	}

	manifests := ExtractManifestObjects(jo)
	SortManifests(manifests)
	return manifests, err
}

func YAMLManifests(manifests []interface{}) (string, error) {
	var errors error

	buf := new(bytes.Buffer)
	encoder := yaml.NewEncoder(buf)

	for _, manifest := range manifests {
		err := encoder.Encode(manifest)
		if err != nil {
			errors = multierror.Append(errors, err)
		}
	}

	return string(buf.Bytes()), errors
}
