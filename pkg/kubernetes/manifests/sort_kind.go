package manifests

import (
	"sort"

	"github.com/stretchr/objx"
)

var kindOrder = []string{
	"Namespace",
	"NetworkPolicy",
	"ResourceQuota",
	"LimitRange",
	"PodSecurityPolicy",
	"PodDisruptionBudget",
	"ServiceAccount",
	"Secret",
	"ConfigMap",
	"StorageClass",
	"PersistentVolume",
	"PersistentVolumeClaim",
	"CustomResourceDefinition",
	"ClusterRole",
	"ClusterRoleList",
	"ClusterRoleBinding",
	"ClusterRoleBindingList",
	"Role",
	"RoleList",
	"RoleBinding",
	"RoleBindingList",
	"Service",
	"DaemonSet",
	"Pod",
	"ReplicationController",
	"ReplicaSet",
	"Deployment",
	"HorizontalPodAutoscaler",
	"StatefulSet",
	"Job",
	"CronJob",
	"Ingress",
	"APIService",
}

// Sort orders manifests in a stable order, taking order-dependencies of these
// into consideration. This is best-effort based:
// - Use the static kindOrder list if possible
// - Sort alphabetically by kind otherwise
// - If kind equal, sort alphabetically by name
func SortManifests(list []interface{}) {
	sort.SliceStable(list, func(i int, j int) bool {
		var io, jo int

		li := objx.New(list[i])
		lj := objx.New(list[j])

		// anything that is not in kindOrder will get to the end of the install list.
		for io = 0; io < len(kindOrder); io++ {
			if li.Get("kind").String() == kindOrder[io] {
				break
			}
		}

		for jo = 0; jo < len(kindOrder); jo++ {
			if lj.Get("kind").String() == kindOrder[jo] {
				break
			}
		}

		// If Kind of both objects are at different indexes of kindOrder, sort by them
		if io != jo {
			return io < jo
		}

		// If the Kinds themselves are different (e.g. both of the Kinds are not in
		// the kindOrder), order alphabetically.
		if ik, jk := li.Get("kind").String(), lj.Get("kind").String(); ik != jk {
			return ik < jk
		}

		// If namespaces differ, sort by the namespace.
		if ins, jns := li.Get("metadata.namespace").String(), lj.Get("metadata.namespace").String(); ins != jns {
			return ins < jns
		}

		// Otherwise, order the objects by name.
		return li.Get("metadata.name").String() < lj.Get("metadata.name").String()
	})
}
