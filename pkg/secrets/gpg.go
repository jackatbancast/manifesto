package secrets

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

const (
	DefaultKubernetesSecretName = "argocd/manifesto-gpg"
)

func FromSecret(secret *corev1.Secret) (openpgp.EntityList, error) {
	privKey, ok := secret.Data["privateKey"]
	if !ok {
		return nil, fmt.Errorf("failed to read private key")
	}

	passphrase, _ := secret.Data["passphrase"]

	return FromString(privKey, passphrase)
}

func FromString(privKey []byte, passphrase []byte) (openpgp.EntityList, error) {
	buf := bytes.NewReader(privKey)

	keyring, err := openpgp.ReadArmoredKeyRing(buf)
	if err != nil {
		return nil, fmt.Errorf("failed to load keyring: %w", err)
	}

	// Try and unlock the keys
	DecryptKeys(keyring, passphrase)

	return keyring, nil
}

func DecryptKeys(keys openpgp.EntityList, passphrase []byte) {
	// Skip decryption if there is no passphrase
	if len(passphrase) == 0 {
		return
	}

	for _, entity := range keys {
		entity.PrivateKey.Decrypt(passphrase)
		for _, subkey := range entity.Subkeys {
			subkey.PrivateKey.Decrypt(passphrase)
		}
	}
}

func EncryptSecret(keys openpgp.EntityList, secret string) ([]byte, error) {
	buf := new(bytes.Buffer)
	w, err := openpgp.Encrypt(buf, keys, nil, nil, nil)
	if err != nil {
		return nil, err
	}
	_, err = w.Write([]byte(secret))
	if err != nil {
		return nil, err
	}
	err = w.Close()
	if err != nil {
		return nil, err
	}

	bytes, err := ioutil.ReadAll(buf)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func DecryptSecret(keys openpgp.EntityList, secret io.Reader) (string, error) {
	md, err := openpgp.ReadMessage(secret, keys, nil, nil)
	if err != nil {
		return "", err
	}
	bytes, err := ioutil.ReadAll(md.UnverifiedBody)
	if err != nil {
		return "", err
	}
	decStr := string(bytes)
	return decStr, nil
}

func DecryptSecretFile(ctx context.Context, kubeClient *kubernetes.Clientset, kubernetesSecret string, filePath string) (string, error) {
	// Get the keyring secret
	secret, err := GetKubeSecret(ctx, kubeClient, kubernetesSecret)
	if err != nil {
		return "", err
	}

	// Load the keyring from the secret
	keyring, err := FromSecret(secret)
	if err != nil {
		return "", fmt.Errorf("failed to load keyring: %w", err)
	}

	// Open the secret file
	f, err := os.Open(filePath)
	if err != nil {
		return "", err
	}

	fBytes, err := ioutil.ReadAll(f)
	if err != nil {
		return "", err
	}

	var fReader io.Reader
	block, err := armor.Decode(bytes.NewReader(fBytes))
	if err == nil {
		fReader = block.Body
	} else {
		fReader = bytes.NewReader(fBytes)
	}

	// Try and decrypt the secret
	sec, err := DecryptSecret(keyring, fReader)
	if err != nil {
		return "", err
	}

	return sec, nil
}

func GetKubeSecret(ctx context.Context, kube *kubernetes.Clientset, secretPath string) (*corev1.Secret, error) {
	namespace, secretName, err := GetSecretPath(secretPath)
	if err != nil {
		return nil, fmt.Errorf("failed to get path to secret in cluster")
	}

	secret, err := kube.CoreV1().Secrets(namespace).Get(ctx, secretName, metav1.GetOptions{})
	if err != nil {
		return nil, fmt.Errorf("failed to get gpg secret from kubernetes: %w", err)
	}

	return secret, nil
}

func GetSecretPath(secretPath string) (string, string, error) {
	path := strings.SplitN(secretPath, "/", 2)

	switch len(path) {
	case 1:
		return "", path[0], nil
	case 2:
		return path[0], path[1], nil
	default:
		return "", "", fmt.Errorf("invalid secret path passed: %s", path)
	}
}
