package jsonnet

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"

	"gitlab.com/jackatbancast/manifesto/pkg/jsonnetenv"
)

func NewEvalCmd(jpaths *[]string, isSnapshot *bool, extVarStrings *[]string, tlaVarStrings *[]string) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "eval",
		Short: "Evaluate a jsonnet file",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("must provide one file to evaluate")
			}

			filename := args[0]

			_, err := os.Open(filename)
			if err != nil {
				return fmt.Errorf("failed to open file %s: %w", filename, err)
			}
			return nil
		},
	}

	asString := cmd.PersistentFlags().BoolP("string", "s", false, "Output a string value directly")

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		output, err := Eval(cmd.Context(), args[0], *asString, *jpaths, *isSnapshot, extVarStrings, tlaVarStrings)
		if err != nil {
			return err
		}
		fmt.Print(output)
		return nil
	}

	return cmd
}

func Eval(ctx context.Context, filename string, asString bool, jpaths []string, isSnapshot bool, extVarStrings *[]string, tlaVarStrings *[]string) (string, error) {
	extVars := ExtractVariables(*extVarStrings)
	tlaVars := ExtractVariables(*tlaVarStrings)
	vm := jsonnetenv.NewVM(ctx, filepath.Dir(filename), isSnapshot, extVars, tlaVars)
	vm.Apply(
		ctx,
		jsonnetenv.WithFileImporter(filename, jpaths),
		jsonnetenv.WithStringOutput(asString),
	)

	return vm.EvaluateFile(ctx, filename)
}
