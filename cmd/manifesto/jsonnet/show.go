package jsonnet

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/jackatbancast/manifesto/pkg/git"
	"gitlab.com/jackatbancast/manifesto/pkg/jsonnetenv"
	manifestsLib "gitlab.com/jackatbancast/manifesto/pkg/kubernetes/manifests"
	"gitlab.com/jackatbancast/manifesto/pkg/registry"
)

func NewShowCmd(jpaths *[]string, isSnapshot *bool, extVarStrings *[]string, tlaVarStrings *[]string) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "show",
		Short: "Evaluate a jsonnet file",
	}

	var repoRoot string
	repoRoot, err := git.GetRepoRoot()
	if err != nil {
		repoRoot = "."
	}
	registryPath := cmd.PersistentFlags().String("registry", filepath.Join(repoRoot, "manifests", "registry.jsonnet"), "Registry to use")
	service := cmd.PersistentFlags().StringP("service", "s", "", "Service to render")
	environment := cmd.PersistentFlags().StringP("environment", "e", "", "Environment to render for given service")
	target := cmd.PersistentFlags().String("target", "", "Target to render")

	// Handle to refer to a target
	var tgt *registry.Target

	cmd.Args = func(cmd *cobra.Command, args []string) error {
		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		// Check we have some arguments
		if len(args) == 0 && (*registryPath == "" || *service == "") {
			return fmt.Errorf("neither file or registry & service provided")
		}

		// Check we don't have too many arguments
		if len(args) == 1 && (*registryPath != "" && *service != "") {
			return fmt.Errorf("can't provide both {service, environment} flags and a filepath")
		}

		// We are handling a file
		if len(args) == 1 {
			_, err := os.Stat(args[0])
			if err != nil {
				return fmt.Errorf("failed to stat file `%s`: %w", err)
			}

			return nil
		}

		// Try and load the registry target
		reg, err := registry.GetRegistry(ctx, *jpaths, *registryPath)
		if err != nil {
			return fmt.Errorf("failed to load registry: %w", err)
		}

		// Find service
		svc := reg.Find(*service)
		if svc == nil {
			serviceList := make([]string, 0, len(reg.Services))
			for _, svc := range reg.Services {
				serviceList = append(serviceList, svc.Meta.Name)
			}
			return fmt.Errorf(
				"failed to find service `%s`. Options are: %s",
				*service,
				strings.Join(serviceList, ", "),
			)
		}

		// Find Environment
		var env *registry.Environment
		if *environment != "" {
			env = svc.Find(*environment)
		} else if len(svc.Spec.Environments) == 1 {
			env = &svc.Spec.Environments[0]
			*environment = env.Meta.Name
		}

		if env == nil {
			envList := make([]string, 0, len(reg.Services))
			for _, env := range svc.Spec.Environments {
				envList = append(envList, env.Meta.Name)
			}
			return fmt.Errorf(
				"failed to find environment `%s`. Options are: %s",
				*service,
				strings.Join(envList, ", "),
			)
		}

		if *target != "" {
			tgt = env.Find("ArgoCD", *target)
		} else if len(env.Spec.Targets) == 1 {
			tgt = &env.Spec.Targets[0]
			*target = tgt.Meta.Name
		}

		if tgt == nil {
			targetList := make([]string, 0, len(reg.Services))
			for _, tgt := range env.Spec.Targets {
				targetList = append(targetList, tgt.Meta.Name)
			}
			return fmt.Errorf(
				"failed to find target `%s`. Options are: %s",
				*target,
				strings.Join(targetList, ", "),
			)
		}

		return nil
	}

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		var filename string
		if len(args) > 0 {
			filename = args[0]
		}

		output, err := Show(cmd.Context(), filename, *registryPath, *jpaths, *isSnapshot, extVarStrings, tlaVarStrings, *service, *environment, *target)
		if err != nil {
			return err
		}
		fmt.Print(output)
		return nil
	}

	return cmd
}

func Show(ctx context.Context, filename string, registryPath string, jpaths []string, isSnapshot bool, extVarStrings *[]string, tlaVarStrings *[]string, service string, environment string, target string) (string, error) {
	extVars := ExtractVariables(*extVarStrings)
	tlaVars := ExtractVariables(*tlaVarStrings)
	vm := jsonnetenv.NewVM(ctx, filepath.Dir(filename), isSnapshot, extVars, tlaVars)
	vm.Apply(
		ctx,
		jsonnetenv.WithFileImporter(filename, jpaths),
	)

	var manifests []interface{}
	var err error
	if filename != "" {
		manifests, err = ShowFile(ctx, vm, filename)
	} else {
		manifests, err = ShowTarget(ctx, vm, isSnapshot, registryPath, jpaths, service, environment, target)
	}
	if err != nil {
		return "", err
	}

	yamlManifests, err := manifestsLib.YAMLManifests(manifests)
	if err != nil {
		return "", err
	}
	return yamlManifests, nil

}

func ShowFile(ctx context.Context, vm *jsonnetenv.VM, filename string) ([]interface{}, error) {
	jsonOutput, err := vm.EvaluateFile(ctx, filename)
	if err != nil {
		return nil, fmt.Errorf("failed to evaluate file (%s): %w", filename, err)
	}

	manifests, err := manifestsLib.ExtractJSONManifests(jsonOutput)
	if err != nil {
		return nil, err
	}

	return manifests, nil
}

func ShowTarget(ctx context.Context, vm *jsonnetenv.VM, isSnapshot bool, registryPath string, jpaths []string, service string, environment string, target string) ([]interface{}, error) {
	var manifestsCollection []interface{}

	repoRoot, err := git.GetRepoRoot()
	if err != nil {
		return nil, err
	}

	reg, err := registry.GetRegistry(ctx, jpaths, registryPath)
	if err != nil {
		return nil, fmt.Errorf("failed to load registry: %w", err)
	}

	// We validated these exist above
	svc := reg.Find(service)
	env := svc.Find(environment)
	tgt := env.Find("ArgoCD", target)

	namespace, ok := tgt.Spec["namespace"].(string)
	if !ok {
		return nil, fmt.Errorf("namespace not set for target %s/%s (%s)", tgt.Meta.Context, tgt.Meta.Name, tgt.Meta.Type)
	}

	filename := filepath.Join(
		repoRoot, "manifests", "services",
		svc.Meta.Name, "instances",
		tgt.Meta.Context, namespace, fmt.Sprintf("%s.jsonnet", tgt.Meta.Name),
	)

	jsonOutput, err := NewVMEvaluate(ctx, jpaths, isSnapshot, filename, []jsonnetenv.Var{
		{Key: "service", Value: svc.Meta.Name},
		{Key: "environment", Value: env.Meta.Name},
		{Key: "namespace", Value: namespace},
		{Key: "release", Value: tgt.Meta.Name},
	}, nil)
	if err != nil {
		return nil, err
	}

	manifests, err := manifestsLib.ExtractJSONManifests(jsonOutput)
	if err != nil {
		return nil, err
	}
	manifestsCollection = append(manifestsCollection, manifests...)

	return manifestsCollection, nil
}

func NewVMEvaluate(ctx context.Context, jpaths []string, isSnapshot bool, path string, extVars []jsonnetenv.Var, tlaVars []jsonnetenv.Var) (string, error) {
	vm := jsonnetenv.NewVM(ctx, filepath.Dir(path), isSnapshot, []jsonnetenv.Var{}, []jsonnetenv.Var{})
	vm.Apply(
		ctx,
		jsonnetenv.WithExtVars(extVars),
		jsonnetenv.WithTLAVars(tlaVars),
		jsonnetenv.WithFileImporter(path, jpaths),
	)

	jsonOutput, err := vm.EvaluateFile(ctx, path)
	if err != nil {
		return "", fmt.Errorf("failed to evaluate file (%s): %w", path, err)
	}

	return jsonOutput, nil
}
