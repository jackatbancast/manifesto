package jsonnet

import (
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/jackatbancast/manifesto/pkg/git"
	"gitlab.com/jackatbancast/manifesto/pkg/jsonnetenv"
)

func NewJsonnetCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "jsonnet",
		Short: "jsonnet functions",
	}

	var repoRoot string
	repoRoot, err := git.GetRepoRoot()
	if err != nil {
		repoRoot = "."
	}

	jpaths := cmd.PersistentFlags().StringSliceP("library", "J", []string{filepath.Join(repoRoot, "manifests", "lib")}, "JPaths to search for libraries in")
	isSnapshot := cmd.PersistentFlags().Bool("snapshot", false, "Indicate that this is a snapshot and secret values should be hidden")
	extVars := cmd.PersistentFlags().StringSliceP("ext-str", "V", []string{}, "External string support for std.ExtVar(...)")
	tlaVars := cmd.PersistentFlags().StringSliceP("tla-str", "A", []string{}, "External string support for top level arguments")

	cmd.AddCommand(NewEvalCmd(jpaths, isSnapshot, extVars, tlaVars))
	cmd.AddCommand(NewShowCmd(jpaths, isSnapshot, extVars, tlaVars))
	return cmd
}

func ExtractVariables(vars []string) []jsonnetenv.Var {
	variables := make([]jsonnetenv.Var, 0, len(vars))
	for _, variable := range vars {
		// key=value
		parts := strings.SplitN(variable, "=", 2)
		if len(parts) != 2 {
			// TODO(jackatbancast): add warnings if this is hit
			continue
		}
		variables = append(variables, jsonnetenv.Var{Key: parts[0], Value: parts[1]})
	}

	return variables
}
