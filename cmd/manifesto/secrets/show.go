package secrets

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/jackatbancast/manifesto/pkg/kubernetes"
	"gitlab.com/jackatbancast/manifesto/pkg/secrets"
)

func NewShowCmd(kubernetesContext *string, kubernetesSecret *string) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "show",
		Short: "Show the value from a stored secret",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("must provide one file to evaluate")
			}

			filename := args[0]

			_, err := os.Open(filename)
			if err != nil {
				return fmt.Errorf("failed to open file %s: %w", filename, err)
			}
			return nil
		},
	}

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		client, err := kubernetes.GetKubeClient(*kubernetesContext)
		if err != nil {
			return err
		}

		output, err := secrets.DecryptSecretFile(cmd.Context(), client, *kubernetesSecret, args[0])
		if err != nil {
			return err
		}
		fmt.Print(output)
		return nil
	}

	return cmd
}
