package secrets

import (
	"github.com/spf13/cobra"
	"gitlab.com/jackatbancast/manifesto/pkg/secrets"
)

func NewSecretsCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "secrets",
		Short: "secret functions! ssssshhhhhhhh!",
	}

	kubernetesContext := cmd.PersistentFlags().String("context", "", "Kubernetes context to get secret from")
	kubernetesSecret := cmd.PersistentFlags().String("secret", secrets.DefaultKubernetesSecretName, "[namespace/]name to use for when en/decryping secrets")

	cmd.AddCommand(NewShowCmd(kubernetesContext, kubernetesSecret))
	return cmd
}
