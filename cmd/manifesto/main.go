package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/jackatbancast/manifesto/cmd/manifesto/jsonnet"
	"gitlab.com/jackatbancast/manifesto/cmd/manifesto/secrets"
	"gitlab.com/jackatbancast/manifesto/cmd/manifesto/snapshot"
	"gitlab.com/jackatbancast/manifesto/cmd/manifesto/template"
)

func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "manifesto",
		Short: "Manifesto is a tool to assist managing application deployments in jsonnet",
	}

	cmd.AddCommand(jsonnet.NewJsonnetCmd())
	cmd.AddCommand(secrets.NewSecretsCmd())
	cmd.AddCommand(snapshot.NewSnapshotCmd())
	cmd.AddCommand(template.NewTemplateCmd())
	return cmd
}

func Execute() {
	rootCmd := NewRootCmd()

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
