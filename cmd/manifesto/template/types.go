package template

import "github.com/stretchr/objx"

type Template struct {
	Metadata TemplateMetadata `json:"metadata"`
	Spec     TemplateSpec     `json:"spec"`
}

type TemplateMetadata struct {
	Name       string              `json:"name"`
	Maintainer string              `json:"maintainer"`
	Parameters []TemplateParameter `json:"parameters"`
}

type TemplateSpec struct {
	Sources []TemplateSource `json:"sources"`
}

type TemplateParameter struct {
	Name     string              `json:"name"`
	Type     string              `json:"type"`
	Default  interface{}         `json:"default,omitempty"`
	Required bool                `json:"required"`
	Children []TemplateParameter `json:"children,omitempty"`
}

type TemplateSource struct {
	Metadata SourceMetadata `json:"metadata"`
	Spec     objx.Map       `json:"spec"`
}

type SourceMetadata struct {
	Type         string               `json:"type"`
	Target       string               `json:"target"`
	Conditions   []bool               `json:"conditions"`
	Dependencies []SourceDependencies `json:"dependencies"`
}

type SourceDependencies struct{}
