package template

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"text/template"

	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"

	"gitlab.com/jackatbancast/manifesto/pkg/git"
	"gitlab.com/jackatbancast/manifesto/pkg/jsonnetenv"
)

func NewGenerateCmd(jpaths *[]string, isSnapshot *bool, extVarStrings *[]string, extCodeStrings *[]string, tlaVarStrings *[]string) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "generate",
		Short: "Generate configuration for our service registry",
	}

	var repoRoot string
	repoRoot, err := git.GetRepoRoot()
	if err != nil {
		repoRoot = "."
	}

	templateName := cmd.PersistentFlags().String("template", "default", "Name of the template to use")
	templatePath := cmd.PersistentFlags().String("template-path", filepath.Join(repoRoot, "manifests", "templates"), "location of the templates")
	overwrite := cmd.PersistentFlags().Bool("overwrite", false, "Overwrite files if they already exist")
	service := cmd.PersistentFlags().StringP("service", "s", "", "Service to render")

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		if *service == "" {
			return fmt.Errorf("must provide a --service")
		}
		err := Generate(cmd.Context(), *jpaths, extVarStrings, extCodeStrings, tlaVarStrings, *templatePath, *templateName, *service, *overwrite, repoRoot)
		if err != nil {
			return err
		}
		return nil
	}

	return cmd
}

const (
	parametersSnippetFormat string = `
(import '%s') {
  spec+: {
    sources: [],
  },
}
`
)

func Generate(ctx context.Context, jpaths []string, extVarStrings *[]string, extCodeStrings *[]string, tlaVarStrings *[]string, templatePath string, templateName string, service string, overwrite bool, repoRoot string) error {
	generateRoot := func(components ...string) string {
		return filepath.Join(append([]string{repoRoot, "manifests", "services", service}, components...)...)
	}
	templateRoot := func(components ...string) string {
		return filepath.Join(append([]string{templatePath, templateName}, components...)...)
	}

	// If we don't have a root directory to copy into then make one
	_, err := os.Stat(generateRoot())
	if os.IsNotExist(err) {
		os.Mkdir(generateRoot(), os.ModePerm)
	}

	extVars := ExtractVariables(*extVarStrings)
	extCodes := ExtractVariables(*extVarStrings)
	tlaVars := ExtractVariables(*tlaVarStrings)
	filename := filepath.Join(templatePath, templateName, "metadata.jsonnet")

	parameterSnippet := fmt.Sprintf(parametersSnippetFormat, filename)
	metadataOutput, err := NewVMEvaluateSnippet(ctx, jpaths, false, parameterSnippet, extVars, extCodes, tlaVars)
	if err != nil {
		return fmt.Errorf("failed to read metadata for template `%s`: %w", templateName, err)
	}

	var metadata Template
	json.Unmarshal([]byte(metadataOutput), &metadata)

	var params map[string]interface{} = map[string]interface{}{
		"Service": service,
	}

	// Update params with new values
	paramValues, err := readParameterList(metadata.Metadata.Parameters)
	if err != nil {
		return fmt.Errorf("failed to read parameters: %w", err)
	}

	// Provide params to the template
	params["Params"] = paramValues

	// extCodes = []jsonnetenv.Var{
	// 	{Key: "RENDER_SOURCES", Value: "true"},
	// }
	// Provide params to the second render of our template metadata
	for pKey, pValue := range paramValues {
		pByteValue, err := json.Marshal(pValue)
		if err != nil {
			return fmt.Errorf(
				"failed to marshal parameter %s: %w",
				pKey,
				err,
			)
		}
		extCodes = append(extCodes, jsonnetenv.Var{
			Key:   fmt.Sprintf("Param::%s", pKey),
			Value: string(pByteValue),
		})
	}

	// Generate a second time to include the parameters in the render
	metadataOutput, err = NewVMEvaluate(ctx, jpaths, false, filename, extVars, extCodes, tlaVars)
	if err != nil {
		return fmt.Errorf("failed to read metadata for template `%s` with provided parameters: %w", templateName, err)
	}
	json.Unmarshal([]byte(metadataOutput), &metadata)

	for _, source := range metadata.Spec.Sources {
		_, err := os.Stat(generateRoot(source.Metadata.Target))
		if !os.IsNotExist(err) {
			if !overwrite {
				fmt.Printf("Skipping (%s): Location already exists\n", source.Metadata.Target)
				continue
			}

			fmt.Printf("Overwriting (%s)\n", source.Metadata.Target)
		}

		if !allConditions(source.Metadata.Conditions...) {
			fmt.Printf("Skipping (%s): At least one false condition found\n", source.Metadata.Target)
			continue
		}

		fmt.Printf("Rendering: %s\n", source.Metadata.Target)

		switch source.Metadata.Type {
		case "Directory":
			err = os.Mkdir(generateRoot(source.Metadata.Target), os.ModePerm)
			if err != nil {
				return fmt.Errorf(
					"failed to create directory target (%s): %w",
					source.Metadata.Target, err,
				)
			}
		case "File":
			fSource, err := os.Open(templateRoot(source.Spec.Get("source").Str()))
			if err != nil {
				return fmt.Errorf("failed to open source file: %s", source.Spec.Get("source").Str())
			}
			defer fSource.Close()
			fTarget, err := os.Create(generateRoot(source.Metadata.Target))
			if err != nil {
				return fmt.Errorf("failed to open target file: %s", generateRoot(source.Metadata.Target))
			}
			defer fTarget.Close()
			_, err = io.Copy(fTarget, fSource)
			if err != nil {
				return fmt.Errorf(
					"failed to copy file (%s) to target (%s): %w",
					source.Spec.Get("source").Str(),
					source.Metadata.Target,
					err,
				)
			}
		case "GoTemplate":
			tplSourcePath := templateRoot(source.Spec.Get("source").Str())
			tplSource, err := os.Open(tplSourcePath)
			if err != nil {
				return fmt.Errorf("failed to open template (%s): %w", tplSourcePath, err)
			}
			defer tplSource.Close()
			data, err := ioutil.ReadAll(tplSource)
			if err != nil {
				return fmt.Errorf("failed to read template (%s): %w", tplSourcePath, err)
			}

			t, err := template.New(tplSourcePath).Parse(string(data))
			if err != nil {
				return fmt.Errorf("failed to load template: %w", err)
			}
			fTarget, err := os.Create(generateRoot(source.Metadata.Target))
			if err != nil {
				return fmt.Errorf("failed to open target (%s) for writing: %w", generateRoot(source.Metadata.Target), err)
			}
			err = t.Execute(fTarget, params)
			if err != nil {
				return fmt.Errorf("failed to render out go template for target (%s): %w", generateRoot(source.Metadata.Target), err)
			}
		}
	}

	return nil
}

func allConditions(conditions ...bool) bool {
	for _, condition := range conditions {
		if !condition {
			return false
		}
	}
	return true
}

func readParameterList(parameterList []TemplateParameter) (map[string]interface{}, error) {
	params := map[string]interface{}{}

	for _, parameter := range parameterList {
		var retval interface{}

		switch parameter.Type {
		case "Group":
			entries := []map[string]interface{}{}

			for {
				prompt := promptui.Select{
					Label: fmt.Sprintf("Add entry to `%s` list?", parameter.Name),
					Items: []string{"Yes", "No"},
				}

				_, result, err := prompt.Run()

				if err != nil {
					fmt.Printf("Prompt failed %v\n", err)
					return nil, fmt.Errorf("failed to read choice (%s)", parameter.Name)
				}

				if result == "No" {
					break
				}

				paramGroup, err := readParameterList(parameter.Children)
				if err != nil {
					return nil, fmt.Errorf("failed to read parameter group (%s): %w", parameter.Name, err)
				}

				entries = append(entries, paramGroup)
			}

			retval = entries
		case "Boolean":
			prompt := promptui.Select{
				Label: parameter.Name,
				Items: []string{"True", "False"},
			}

			_, result, err := prompt.Run()
			if err != nil {
				return nil, fmt.Errorf("failed to get bool answer for (%s): %w", parameter.Name, err)
			}

			retval = result == "True"

		case "Integer":
			pDefault := ""

			if parameter.Default != nil {
				switch p := parameter.Default.(type) {
				case string:
					pDefault = p
				case int:
					pDefault = strconv.FormatInt(int64(p), 10)
				}
			}

			prompt := promptui.Prompt{
				Label:   parameter.Name,
				Default: pDefault,
			}

			result, err := prompt.Run()

			if err != nil {
				return nil, fmt.Errorf("failed to get int result for (%s): %w", result, err)
			}

			retval, err = strconv.Atoi(result)
			if err != nil {
				return nil, fmt.Errorf(
					"requires an integer for (%s) got something else (%s): %w",
					parameter.Name,
					result,
					err,
				)
			}
		default: // And now for everything else
			paramDefault, ok := parameter.Default.(string)
			if !ok {
				paramDefault = ""
			}
			prompt := promptui.Prompt{
				Label:   parameter.Name,
				Default: paramDefault,
				// Validate: validate,
			}
			result, err := prompt.Run()
			if err != nil {
				return nil, fmt.Errorf("failed to read parameter (%s): %w", parameter.Name, err)
			}

			if parameter.Required && result == "" {
				return nil, fmt.Errorf("required parameter not set (%s)", parameter.Name)
			}

			retval = result
		}

		params[parameter.Name] = retval
	}

	return params, nil
}

func NewVMEvaluate(ctx context.Context, jpaths []string, isSnapshot bool, path string, extVars []jsonnetenv.Var, extCodes []jsonnetenv.Var, tlaVars []jsonnetenv.Var) (string, error) {
	vm := jsonnetenv.NewVM(ctx, filepath.Dir(path), isSnapshot, []jsonnetenv.Var{}, []jsonnetenv.Var{})
	vm.Apply(
		ctx,
		jsonnetenv.WithExtVars(extVars),
		jsonnetenv.WithExtCodes(extCodes),
		jsonnetenv.WithTLAVars(tlaVars),
		jsonnetenv.WithFileImporter(path, jpaths),
	)

	jsonOutput, err := vm.EvaluateFile(ctx, path)
	if err != nil {
		return "", fmt.Errorf("failed to evaluate file (%s): %w", path, err)
	}

	return jsonOutput, nil
}

func NewVMEvaluateSnippet(ctx context.Context, jpaths []string, isSnapshot bool, snippet string, extVars []jsonnetenv.Var, extCodes []jsonnetenv.Var, tlaVars []jsonnetenv.Var) (string, error) {
	path, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf("failed to get current working directory: %w", err)
	}

	vm := jsonnetenv.NewVM(ctx, filepath.Dir(path), isSnapshot, []jsonnetenv.Var{}, []jsonnetenv.Var{})
	vm.Apply(
		ctx,
		jsonnetenv.WithExtVars(extVars),
		jsonnetenv.WithExtCodes(extCodes),
		jsonnetenv.WithTLAVars(tlaVars),
		jsonnetenv.WithFileImporter(path, jpaths),
	)

	jsonOutput, err := vm.EvaluateAnonymousSnippet(path, snippet)
	if err != nil {
		return "", fmt.Errorf("failed to evaluate file (%s): %w", path, err)
	}

	return jsonOutput, nil
}
