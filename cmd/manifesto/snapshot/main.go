package snapshot

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"github.com/hashicorp/go-multierror"
	"github.com/spf13/cobra"
	"gitlab.com/jackatbancast/manifesto/pkg/git"
	"gitlab.com/jackatbancast/manifesto/pkg/jsonnetenv"
	"gitlab.com/jackatbancast/manifesto/pkg/kubernetes/manifests"
	registryLib "gitlab.com/jackatbancast/manifesto/pkg/registry"
)

func NewSnapshotCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "snapshot",
		Short: "snapshot manifests in the registry",
	}

	repoRoot := "."
	if gRoot, err := git.GetRepoRoot(); err == nil {
		repoRoot = gRoot
	}

	registryPath := cmd.PersistentFlags().String("registry", filepath.Join(repoRoot, "manifests", "registry.jsonnet"), "Path to the registry file")
	jpaths := cmd.PersistentFlags().StringSliceP("library", "J", []string{filepath.Join(repoRoot, "manifests", "lib")}, "JPaths to search for libraries in")
	service := cmd.PersistentFlags().StringP("service", "s", "", "Service to render, e.g. nginx")
	environment := cmd.PersistentFlags().StringP("environment", "e", "", "Environement to render, e.g. k3s")
	extVars := cmd.PersistentFlags().StringSliceP("ext-str", "V", []string{}, "External string support for std.ExtVar(...)")
	tlaVars := cmd.PersistentFlags().StringSliceP("tla-str", "A", []string{}, "External string support for top level arguments")
	workers := cmd.PersistentFlags().IntP("workers", "c", runtime.NumCPU()/2, "Number of workers to concurrently generate manifests")

	cmd.RunE = func(cmd *cobra.Command, args []string) error {

		err := Snapshot(cmd.Context(), *jpaths, ExtractVars(*extVars), ExtractVars(*tlaVars), *workers, *registryPath, *service, *environment)
		if err != nil {
			return err
		}

		return nil
	}
	return cmd
}

type workEntry struct {
	service     registryLib.Service
	environment registryLib.Environment
	target      registryLib.Target
}

func Snapshot(ctx context.Context, jpaths []string, extVars []jsonnetenv.Var, tlaVars []jsonnetenv.Var, workers int, registryPath string, serviceName string, environmentName string) error {
	jsonOutput, err := NewVMEvaluate(ctx, jpaths, registryPath, extVars, tlaVars)
	if err != nil {
		return err
	}

	// Deserialise registry in to type
	var registry registryLib.Registry
	err = json.Unmarshal([]byte(jsonOutput), &registry)
	if err != nil {
		return err
	}

	// Create queues to process this work
	workQueue := make(chan workEntry)
	errCollect := make(chan error)
	var wg sync.WaitGroup

	for i := 0; i < workers; i++ {
		go snapshotWorker(ctx, &wg, workQueue, errCollect, jpaths, extVars, tlaVars)
	}

	applyToArgoCDTarget := func(f func(registryLib.Service, registryLib.Environment, registryLib.Target)) {
		for _, service := range registry.Services {
			if serviceName != "" && service.Meta.Name != serviceName {
				continue
			}
			for _, environment := range service.Spec.Environments {
				if environmentName != "" && environment.Meta.Name != environmentName {
					continue
				}

				for _, target := range environment.Spec.Targets {
					// Only handle argocd deployment targets in
					// snapshots until we have a better way to
					// handle multiple types of snapshots.
					if target.Meta.Type != "ArgoCD" {
						continue
					}

					namespace := target.Spec.Get("namespace")
					if !namespace.IsStr() {
						continue
					}

					// Run our function after all of this validation
					f(service, environment, target)
				}
			}
		}
	}
	applyToArgoCDTarget(func(service registryLib.Service, environment registryLib.Environment, target registryLib.Target) {
		wg.Add(1)
	})

	// Async add entries to the queue
	go applyToArgoCDTarget(func(service registryLib.Service, environment registryLib.Environment, target registryLib.Target) {
		workQueue <- workEntry{
			service:     service,
			environment: environment,
			target:      target,
		}
	})

	// When all of the work is done we want to close our queues to die gracefully
	go func() {
		wg.Wait()
		close(workQueue)
		close(errCollect)
	}()

	var errors error
	for err := range errCollect {
		errors = multierror.Append(errors, err)
	}

	return errors
}

func snapshotWorker(ctx context.Context, wg *sync.WaitGroup, workQueue <-chan workEntry, errCollect chan<- error, jpaths []string, extVars []jsonnetenv.Var, tlaVars []jsonnetenv.Var) {
	for entry := range workQueue {
		err := RenderTarget(ctx, jpaths, extVars, tlaVars, entry.service, entry.environment, entry.target)
		if err != nil {
			errCollect <- err
		}
		wg.Done()
	}
}

func RenderTarget(ctx context.Context, jpaths []string, extVars []jsonnetenv.Var, tlaVars []jsonnetenv.Var, service registryLib.Service, environment registryLib.Environment, target registryLib.Target) error {
	repoRoot, err := git.GetRepoRoot()
	if err != nil {
		return fmt.Errorf("failed to get git repo root: %w", err)
	}

	namespace := target.Spec.Get("namespace")
	if !namespace.IsStr() {
		return fmt.Errorf(
			"namespace not set for target %s/%s (%s)\n",
			target.Meta.Context, target.Meta.Name, target.Meta.Type,
		)
	}

	filename := filepath.Join(
		repoRoot, "manifests", "services",
		service.Meta.Name, "instances",
		target.Meta.Context, namespace.Str(),
		fmt.Sprintf("%s.jsonnet", target.Meta.Name),
	)
	fmt.Printf("processing jsonnet %s\n", filename)

	renderJSON, err := NewVMEvaluate(
		ctx, jpaths,
		filename,
		append(
			extVars,
			[]jsonnetenv.Var{
				{Key: "service", Value: service.Meta.Name},
				{Key: "environment", Value: environment.Meta.Name},
				{Key: "namespace", Value: namespace.Str()},
				{Key: "release", Value: target.Meta.Name},
			}...,
		),
		tlaVars,
	)
	if err != nil {
		return fmt.Errorf(
			"failed to evaluate jsonnet into json for %s: %w",
			filename, err,
		)
	}

	ms, err := manifests.ExtractJSONManifests(renderJSON)
	if err != nil {
		return fmt.Errorf(
			"failed to extract manifests for %s",
			filename,
		)
	}

	yms, err := manifests.YAMLManifests(ms)
	if err != nil {
		return fmt.Errorf(
			"failed to extract YAML manifests for %s",
			filename,
		)
	}

	yamlFilePath := filepath.Join(
		repoRoot, "manifests", "services",
		service.Meta.Name, "instances",
		target.Meta.Context, namespace.Str(), fmt.Sprintf("%s.yaml", target.Meta.Name),
	)
	f, err := os.Create(yamlFilePath)
	if err != nil {
		return fmt.Errorf("failed to open YAML file for writing: %w", err)
	}
	defer f.Close()

	_, err = f.WriteString(yms)
	if err != nil {
		return fmt.Errorf("failed to write out YAML file: %w", err)
	}

	return nil
}

func NewVMEvaluate(ctx context.Context, jpaths []string, path string, extVars []jsonnetenv.Var, tlaVars []jsonnetenv.Var) (string, error) {
	vm := jsonnetenv.NewVM(ctx, filepath.Dir(path), true, []jsonnetenv.Var{}, []jsonnetenv.Var{})
	vm.Apply(
		ctx,
		jsonnetenv.WithExtVars(extVars),
		jsonnetenv.WithTLAVars(tlaVars),
		jsonnetenv.WithFileImporter(path, jpaths),
	)

	jsonOutput, err := vm.EvaluateFile(ctx, path)
	if err != nil {
		return "", fmt.Errorf("failed to evaluate file (%s): %w", path, err)
	}

	return jsonOutput, nil
}

func ExtractVars(vars []string) []jsonnetenv.Var {
	variables := make([]jsonnetenv.Var, 0, len(vars))
	for _, variable := range vars {
		// key=value
		parts := strings.SplitN(variable, "=", 2)
		if len(parts) != 2 {
			// TODO(jackatbancast): add warnings if this is hit
			continue
		}
		variables = append(variables, jsonnetenv.Var{Key: parts[0], Value: parts[1]})
	}

	return variables
}
