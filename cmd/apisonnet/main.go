package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/jackatbancast/manifesto/cmd/apisonnet/serve"
)

func NewRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "apisonnet",
		Short: "serve jsonnet to the world",
	}

	cmd.AddCommand(serve.NewServeCmd())
	return cmd
}

func main() {
	rootCmd := NewRootCmd()

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
