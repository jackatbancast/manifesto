package serve

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/jackatbancast/manifesto/pkg/jsonnetenv"
)

func NewServeCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "serve",
		Short: "serve the jsonnet files",
	}

	jpaths := cmd.PersistentFlags().StringSliceP("library", "J", []string{"lib"}, "JPaths to search for libraries in")
	configFile := cmd.PersistentFlags().String("config", "config.jsonnet", "Config file to load")

	cmd.RunE = func(cmd *cobra.Command, args []string) error {
		err := Serve(cmd.Context(), *configFile, *jpaths)
		if err != nil {
			return err
		}
		return nil
	}

	return cmd
}

type Config struct {
	MountPoints map[string]MountPoint `json:"mount_points"`
	Address     string                `json:"address"`
}

type MountPoint struct {
	Filename *string `json:"filename,omitempty"`
	Snippet  *string `json:"snippet,omitempty"`
	IsString bool    `json:"is_string"`
}

func Serve(ctx context.Context, configPath string, jpaths []string) error {
	basepath, err := os.Getwd()

	// Create VM
	vm := jsonnetenv.NewVM(ctx, basepath, false, nil, nil)
	vm.Apply(
		ctx,
		jsonnetenv.WithFileImporter(basepath, jpaths),
	)

	// Evaluate our root jsonnet config
	output, err := vm.EvaluateFile(ctx, configPath)
	if err != nil {
		return fmt.Errorf("failed to read the config file: %w", err)
	}
	var index Config
	err = json.NewDecoder(strings.NewReader(output)).Decode(&index)
	if err != nil {
		return fmt.Errorf("failed to decode the config file: %w", err)
	}

	// Create the memory store so we can handle state
	store := NewMemoryStore()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// lookup the entry from the routing table and error
		// if it doesn't exist
		entry, ok := index.MountPoints[r.URL.Path]
		if !ok {
			http.Error(w, "entry doesn't exist", http.StatusNotFound)
			return
		}

		requestVars := make([]jsonnetenv.Var, 0, len(r.Header))
		for key, val := range r.Header {
			for _, valInstance := range val {
				requestVars = append(requestVars, jsonnetenv.Var{
					Key:   fmt.Sprintf("header|%s", key),
					Value: valInstance,
				})
			}
		}

		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err == nil {
			requestVars = append(requestVars, jsonnetenv.Var{
				Key:   "request|body",
				Value: string(bodyBytes),
			})
		}

		ctx := r.Context()
		vm := jsonnetenv.NewVM(ctx, basepath, false, nil, nil)
		vm.Apply(
			ctx,
			jsonnetenv.WithFileImporter(basepath, jpaths),
			jsonnetenv.WithStringOutput(entry.IsString),
			jsonnetenv.WithExtVars(requestVars),
			jsonnetenv.WithHTTPGet(),
			WithStoreGet(store),
			WithStorePut(store),
		)

		if entry.Snippet != nil {
			output, err := vm.EvaluateSnippet(ctx, r.URL.Path, *entry.Snippet)
			if err != nil {
				http.Error(w, "failed to evaluate snippet", http.StatusInternalServerError)
				return
			}

			fmt.Fprint(w, output)
			return
		}

		if entry.Filename != nil {
			output, err := vm.EvaluateFile(ctx, *entry.Filename)
			if err != nil {
				http.Error(w, fmt.Sprintf("failed to eval filename: %s", err), http.StatusInternalServerError)
				return
			}

			fmt.Fprint(w, output)
			return
		}

		http.Error(w, "no jsonnet defined", http.StatusInternalServerError)
	})

	s := &http.Server{
		Addr:           index.Address,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	return s.ListenAndServe()
}
