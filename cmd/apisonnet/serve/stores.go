package serve

import (
	"errors"
	"fmt"

	"gitlab.com/jackatbancast/manifesto/pkg/jsonnetenv"
)

var (
	_ Store = &MemoryStore{}

	ErrObjectNotFound = errors.New("object not found")
)

type Store interface {
	Get(name string) (interface{}, error)
	Put(name string, value interface{}) error
}

func WithStoreGet(store Store) jsonnetenv.VMOption {
	return jsonnetenv.WithNativeFunction("memoryGet", []string{"name"}, func(params []interface{}) (interface{}, error) {
		name, ok := params[0].(string)
		if !ok {
			return nil, fmt.Errorf("failed to parse name")
		}

		val, err := store.Get(name)
		if err != nil && !errors.Is(err, ErrObjectNotFound) {
			return nil, err
		}

		return val, nil
	})
}

func WithStorePut(store Store) jsonnetenv.VMOption {
	return jsonnetenv.WithNativeFunction("memoryPut", []string{"name", "val"}, func(params []interface{}) (interface{}, error) {
		name, ok := params[0].(string)
		if !ok {
			return nil, fmt.Errorf("failed to parse name")
		}

		val := params[1]

		err := store.Put(name, val)
		if err != nil {
			return nil, err
		}

		return "success", nil
	})
}

var _ = MemoryStore{}

type MemoryStore struct {
	storage map[string]interface{}
}

func NewMemoryStore() *MemoryStore {
	return &MemoryStore{
		storage: map[string]interface{}{},
	}
}

func (m *MemoryStore) Get(name string) (interface{}, error) {
	val, ok := m.storage[name]
	if !ok {
		return nil, ErrObjectNotFound
	}
	return val, nil
}

func (m *MemoryStore) Put(name string, val interface{}) error {
	m.storage[name] = val
	return nil
}
