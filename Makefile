PROG_NAME := manifesto
INSTALL_LOCATION := ~/.bin/

all: clean build

.PHONY: clean
clean:
	rm $(PROG_NAME)

.PHONY: build
build:
	bazel build //:manifesto

.PHONY: build-apisonnet
build-apisonnet:
	bazel build //:apisonnet

.PHONY: bazel-gazelle
bazel-gazelle:
	bazel run //:gazelle

.PHONY: bazel-gazelle-update-repos
bazel-gazelle-update-repos:
	bazel run //:gazelle -- update-repos -from_file=go.mod -to_macro=bazel/deps.bzl%go_dependencies

.PHONY: install
install: build
	cp bazel-bin/manifesto ~/.bin/manifesto

.PHONY: install-apisonnet
install-apisonnet: build-apisonnet
	cp bazel-bin/apisonnet ~/.bin/apisonnet
